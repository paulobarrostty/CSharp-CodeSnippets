# CSharp Code Snippets

## Tópicos

- [Como instalar os Code Snippets na sua IDE do Visual Studio](#como-instalar-os-code-snippets-na-sua-ide-do-visual-studio)
- [Como criar um Code Snippet](#como-criar-um-code-snippet)
- [Boas Práticas e documentação](#boas-práticas-e-documentação)
- [Acervo de Code Snippets](#acervo-de-code-snippets)

## Como instalar os Code Snippets na sua IDE do Visual Studio

Baixe este repositório em uma pasta do seu sistema:
```shell
git clone https://gitlab.com/paulobarrostty/CSharp-CodeSnippets.git
cd CSharp-CodeSnippets/
```

Na sua IDE do Visual Studio, vá em **Ferramentas > Gerenciador de Trechos de Código ... >** *Selecione Linguagem "CSharp"* **> Adicionar** então, selecione a pasta **CSharp-CodeSnippet/** no seu explorer. Após feito todo o processo, reinicie sua IDE.

## Como criar um Code Snippet

Para criar um Code Snippet no Visual Studio, você deverá primeiro criar um arquivo XML, nomeando-o com o atalho do seu Snippet e extensão `.snippet`

**Exemplo**:
- Atalho do Snippet: `xpto`
- Nome do arquivo : `xpto.snippet`

Utilize o modelo abaixo para criar seu Snippet:
```xml
<?xml version="1.0" encoding="utf-8"?>  
<CodeSnippets  
    xmlns="http://schemas.microsoft.com/VisualStudio/2005/CodeSnippet">  
    <CodeSnippet Format="1.0.0">  
        <Header>  
            <!-- Titulo do seu CodeSnippet -->
            <Title></Title>
            
            <!-- Descrição do seu CodeSnippet -->
            <Description></Description>
            
            <!-- KeyWord do seu CodeSnippet -->
            <Shortcut></Shortcut>
        </Header>  
        <Snippet>
           <!-- Variáveis que serão utilizadas -->
           <Declarations>
                <Object>
                    <ID>minhaVariavel</ID>
                    <Type>var</Type>
                    <ToolTip>Substitua com valor/expressão que deseja aplicar seu CodeSnippet.</ToolTip>
                    <Default>0</Default>
                </Object>
            </Declarations>  
            
            <Code Language="csharp">  
                <![CDATA[
                    <!-- 
                        Bloco de código que será gerado
                        
                        - Variáveis 
                            Utilize o símbolo $$ para definir uma variável
                                E.g. 'Console.Write($minhaVariavel$);'
                        
                    -->
                ]]>  
            </Code>
            
            <!-- Imports necessários para seu CodeSnippet -->
            <Imports>
                <Import>
                    <Namespace><!-- 'System | System.Utils | XPTO.Classes ' --></Namespace>
                </Import>
            </Imports>
        </Snippet>
    </CodeSnippet>
</CodeSnippets>  
```

## Boas Práticas e documentação

:white_check_mark: Adicione seu Code Snippet na sessão [Acervo de Code Snippets](#acervo-de-code-snippets), contendo:
- O atalho do seu Code Snippet
- A descrição do seu Code nippet
- O link para o arquivo `.snippet` no atalho do seu Code Snippet 

:white_check_mark: Crie Snippets relevantes, que tenham um propósito para existir.

:white_check_mark: Utilize os `<imports>` necessários para funcionamento do seu Code Snippet. 

:white_check_mark: Mantenha seu codigo identado, utilizando seu padrão de preferência.

:white_check_mark: Pesquise antes de criar seu Code Snippet, para evitar redundancia de código.

---

:x: Evite nomes complexos e não relacionados as classes e funções utilizados no seu Code Snippet.

:x: Evite código mal identado e de dificil legibilidade.

---

:tada: Proponha alterações para esse documento 

## Acervo de Code Snippets 

- [cvti](https://gitlab.com/paulobarrostty/CSharp-CodeSnippets/blob/master/cvti.snippet): Converte um valor/instrução para `int`
- [cvtd](https://gitlab.com/paulobarrostty/CSharp-CodeSnippets/blob/master/cvtd.snippet): Converte um valor/instrução para `decimal`
- [cvtb](https://gitlab.com/paulobarrostty/CSharp-CodeSnippets/blob/master/cvtb.snippet): Converte um valor/instrução para `bool`